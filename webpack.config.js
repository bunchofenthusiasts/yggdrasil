﻿var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        polyfills: './src/app/polyfills.ts',
        vendor: './src/app/vendor.ts',
        app: './src/app/main.ts'
    },
    output: {
        path: "./build",
        filename: "./js/[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: true
        }),
        new ExtractTextPlugin('style.css'),
        new CleanWebpackPlugin(['.build'], {verbose: true})
    ],
    resolve: {
        extensions: ["", ".js", ".ts"]
    },

  devServer: {
    historyApiFallback: true
  }

}