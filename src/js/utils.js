function cloneObject(obj)
{
    if (obj == null)
        return null;
    
    if (obj == undefined)
        return undefined;
        
    return jQuery.extend(true, {}, obj);
}