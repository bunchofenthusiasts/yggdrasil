import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardComponent } from './components/auth/authGuard.component';
import { SignInComponent } from './components/auth/signIn.component';
import { SignUpComponent } from './components/auth/signUp.component';
import { HomeComponent } from './components/home.component';
import { DashboardComponent }   from './components/dashboard.component';

import { TrackViewFormComponent }   from './components/forms/track/trackViewForm.component';
import { TrackEditFormComponent }   from './components/forms/track/trackEditForm.component';
import { TrackCreateFormComponent }   from './components/forms/track/trackCreateForm.component';

import { SectionEditFormComponent }   from './components/forms/section/sectionEditForm.component';
import { SectionViewFormComponent }   from './components/forms/section/sectionViewForm.component';
import { SectionCreateFormComponent }   from './components/forms/section/sectionCreateForm.component';

import { SectionLinkViewFormComponent }   from './components/forms/section/link/sectionLinkViewForm.component';
import { SectionLinkEditFormComponent }   from './components/forms/section/link/sectionLinkEditForm.component';
import { SectionLinkCreateFormComponent }   from './components/forms/section/link/sectionLinkCreateForm.component';

import { ErrorPageComponent }   from './components/system/errorPage.component';
import { NotFoundPageComponent }   from './components/system/notFoundPage.component';

import { LocationStrategy } from '@angular/common';

const routes: Routes = 
[
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'signIn',   component: SignInComponent },
  { path: 'signUp',   component: SignUpComponent },
  { path: 'home', component:  HomeComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'dashboard/:id', component: DashboardComponent, canActivate: [ AuthGuardComponent ] },
  
  { path: 'viewTrack/:id', component: TrackViewFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'editTrack/:id', component: TrackEditFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'createTrack', component: TrackCreateFormComponent, canActivate: [ AuthGuardComponent ] },

  { path: 'viewSection/:id', component: SectionViewFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'editSection/:id', component: SectionEditFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'createSection/:trackId', component: SectionCreateFormComponent, canActivate: [ AuthGuardComponent ] },

  { path: 'viewSectionLink/:id', component: SectionLinkViewFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'editSectionLink/:id', component: SectionLinkEditFormComponent, canActivate: [ AuthGuardComponent ] },
  { path: 'createSectionLink/:sectionId', component: SectionLinkCreateFormComponent, canActivate: [ AuthGuardComponent ] },

  { path: 'error', component: ErrorPageComponent },
  { path: 'notFound', component: NotFoundPageComponent },
  { path: '**', redirectTo: '/notFound' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule 
{

}