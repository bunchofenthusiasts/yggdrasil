import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { BaseViewComponent } from './base/baseView.component';
import { AppComponent } from './app.component';
import { BreadCrumb } from '../classes/view/breadCrumbs/breadCrumb';

@Component({
    selector: 'home',
    template: require('../views/home.html')
})

export class HomeComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent)
    {
        super(router, route, location, appComponent);
    }

    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Главная";
        bc.link = "/home";
        bc.type = "home";
        
        return bc;
    }   
}