import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Router } from '@angular/router';
import { User } from '../classes/user'
import { Track } from '../classes/track'
import { TrackService } from '../services/track.service';
import { UserService }  from '../services/user.service';

import { Location } from '@angular/common';
import { AppComponent } from './app.component';
import { BaseViewComponent } from './base/baseView.component';

import { BreadCrumb } from '../classes/view/breadCrumbs/breadCrumb';

@Component({
    selector: 'user-dashboard',
    template: require('../views/userdashboard.html'),
    providers: [TrackService, UserService ]
})

export class DashboardComponent extends BaseViewComponent {

    constructor(public router: Router, public route: ActivatedRoute, public location: Location,
        public appComponent: AppComponent,
        private trackService: TrackService, 
        private userService: UserService) 
        {
            super(router, route, location, appComponent);
        }
    
    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Рабочий стол";
        bc.link = "/dashboard/"+this.currentUserId;
        bc.type = "dashboard";
        return bc;
    }

    @Input()
    get currentUserId(): number
    {
       return this.appComponent.currentUserId;
    }

    tracks: Track[];

    currentTrack: Track;
    
    init(obj:any): any 
    {
        super.init(obj);
        return this.getTracks();
    }

    getTracks(): Promise<any> 
    {
        let userId: number;

        if (!this.currentUserId)
        {    
            this.route.params.subscribe(params => {userId = +params['id']});
        }
        else
        {
             userId = this.currentUserId
        }


        var trackPromise:Promise<any> = this.trackService.getList({ userId: userId});
        trackPromise.then(
                response =>
                {
                    this.tracks = response;
                },
                error =>
                {
                    return this.rejectPromise(error);
        });

        return trackPromise;
    }

    onSelect(track: Track) 
    {
        this.currentTrack = track;
    }

    editTrack(track: Track) 
    {
        this.onSelect(track);
        this.router.navigate(['/viewTrack', track.id]);
    }

    deleteTrack(track: Track): void 
    {
        this.trackService.delete(track.id)
            .then(() => {
                this.tracks = this.tracks.filter(u => u !== track);
            });
    }
    
    createTrack(): void 
    {
        this.router.navigate(['/createTrack']);
    }
}