import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';

import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

import { MakeDraggable } from '../../../utils/dnd/make-draggable.directive';
import { MakeDroppable } from '../../../utils/dnd/make-droppable.directive';

import { ListItem } from '../../../components/base/listItem.component'


@Component({
    selector: 'track-create-form',
    template: require('../../../views/forms/track/trackEditForm.html'),
    providers: [TrackService, SectionService]  
    
})

export class TrackCreateFormComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        private trackService: TrackService, 
        private sectionService: SectionService)
        {
            super(router, route, location, appComponent);
        }

    get currentUserId(): number
    {
        return this.appComponent.currentUserId;
    }

    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Создание новой тропинки";
        bc.link = "/createTrack";
        bc.type = "createTrack";
        return bc;
    }

    track: Track = new Track();

    saveTrack(): void 
    {
        this.track.userId = this.currentUserId;
        this.track.isPassed = false;
        this.trackService.create(this.track).then(() => this.goBack());
    }
}