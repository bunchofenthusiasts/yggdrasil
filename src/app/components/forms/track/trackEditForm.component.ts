import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';

import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

import { MakeDraggable } from '../../../utils/dnd/make-draggable.directive';
import { MakeDroppable } from '../../../utils/dnd/make-droppable.directive';

import { ListItem } from '../../../components/base/listItem.component'


@Component({
    selector: 'track-edit-form',
    template: require('../../../views/forms/track/trackEditForm.html'),
    providers: [TrackService, SectionService]  
    
})

export class TrackEditFormComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        private trackService: TrackService, 
        private sectionService: SectionService)
        {
            super(router, route, location, appComponent);
        }

    track: Track = new Track();

    init(obj:any): any 
    {
        super.init(obj);

        let trackId:number = -1;
        this.route.params.subscribe(params => {trackId = +params['id']});

        let trackPromise:Promise<any> = this.trackService.getById(trackId);

        let sectionsPromise:Promise<any> = trackPromise.then(
            response => 
            {
                this.track = response;
            },
            error => 
            {
                return this.rejectPromise(error);
            });

        return trackPromise;
    }

    saveTrack(): void 
    {
        this.trackService.update(this.track).then(() => this.goBack());
    }
}