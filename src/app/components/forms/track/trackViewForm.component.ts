import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';

import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

import { MakeDraggable } from '../../../utils/dnd/make-draggable.directive';
import { MakeDroppable } from '../../../utils/dnd/make-droppable.directive';

import { ListItem } from '../../../components/base/listItem.component'
import { DNDUtil } from '../../../utils/dnd/dndUtil';


@Component({
    selector: 'track-view-form',
    template: require('../../../views/forms/track/trackViewForm.html'),
    providers: [TrackService, SectionService]  
    
})

export class TrackViewFormComponent extends BaseViewComponent
{
    constructor(public router: Router, public route: ActivatedRoute, public location: Location, public appComponent: AppComponent,
        private trackService: TrackService, 
        private sectionService: SectionService) 
    {
        super(router, route, location, appComponent)
    }

    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = this.track.name;
        bc.link = "/viewTrack/"+this.track.id;
        bc.type = "viewTrack";
        
        return bc;
    }    


    track: Track = new Track();
    trackSections: Section[];

    init(obj:any): any 
    {
        super.init(obj);

        let trackId:number = -1;
        this.route.params.subscribe(params => {trackId = +params['id']});

        let trackPromise:Promise<any> = this.trackService.getById(trackId);

        let sectionsPromise:Promise<any> = trackPromise.then(
            response => 
            {
                this.track = response;
                return this.sectionService.getList({ trackId: trackId});
            },
            error => 
            {
                return this.rejectPromise(error);
            }).then(
                response =>
                {
                    this.trackSections = [];
                    response.forEach(
                        (existedSection:Section) =>
                        {
                            let obj:Section = <Section>existedSection;
                            this.trackSections.push(obj)
                        });
                },
                error => 
                {
                    return this.rejectPromise(error);
                }
            );

        return sectionsPromise;
    }
 
    editTrack():void
    {
        this.router.navigate(['/editTrack', this.track.id]);
    }

    viewSection(section: Section):void
    {
        this.router.navigate(['/viewSection', section.id]);
    }

    createSection():void
    {
        this.router.navigate(['/createSection', this.track.id]);
    }

    deleteSection(section: Section): void 
    {
        let freeOrderNumber:number = section.orderNumber;
        this.trackService.delete(section.id).then(
            () => 
            {
                this.trackSections = this.trackSections.filter(u => u !== section);
                DNDUtil.updateOrderedListAfterDeleteRow(freeOrderNumber, this.trackSections);
            });
    }

    onDrop(src: Section, trg: Section) 
    {
        DNDUtil.moveRowByOrderNumber(this.trackSections, src.orderNumber, trg.orderNumber);
    }
}