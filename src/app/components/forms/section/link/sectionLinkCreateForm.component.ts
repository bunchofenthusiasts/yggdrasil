import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { Section } from '../../../../classes/section'
import { SectionLink } from '../../../../classes/sectionLink'
import { SectionLinkService } from '../../../../services/sectionLink.service';

import { BreadCrumb } from '../../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../../app.component';
import { BaseViewComponent } from '../../../base/baseView.component';

@Component({
    selector: 'section-link-create-form',
    template: require('../../../../views/forms/section/link/sectionLinkEditForm.html'),
    providers: [ SectionLinkService ]
})

export class SectionLinkCreateFormComponent extends BaseViewComponent 
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public sectionLinkService: SectionLinkService)
    {
        super(router, route, location, appComponent);
    }
    
    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Добавление ссылки на материал";
        bc.link = "/createSectionLink";
        bc.type = "createSectionLink";
        return bc;
    } 

    sectionLink: SectionLink = new SectionLink();
    sectionId: number = undefined;
    sectionLinksCount: number = undefined;

    init(obj:any): any 
    {
        super.init(obj);

        this.route.params.subscribe(params => {this.sectionId = +params['sectionId']});

        let sectionLinkPromise = this.sectionLinkService.getList({ sectionId: this.sectionId}).then(
            response => 
            {
                this.sectionLinksCount = response.length;
            },
            error =>
            {
                return this.rejectPromise(error);
            });
        return sectionLinkPromise;

    }
    
    saveSectionLink(): void 
    {
        this.sectionLink.sectionId = this.sectionId;
        this.sectionLink.orderNumber = this.sectionLinksCount; // Перенести на сервер присвоение номера
        this.sectionLinkService.create(this.sectionLink).then(() => this.goBack());
    }
}