import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { Section } from '../../../../classes/section'
import { SectionLink } from '../../../../classes/sectionLink'
import { SectionLinkService } from '../../../../services/sectionLink.service';

import { BreadCrumb } from '../../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../../app.component';
import { BaseViewComponent } from '../../../base/baseView.component';

@Component({
    selector: 'section-link-view-form',
    template: require('../../../../views/forms/section/link/sectionLinkViewForm.html'),
    providers: [ SectionLinkService ]
})

export class SectionLinkViewFormComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public sectionLinkService: SectionLinkService)
    {
        super(router, route, location, appComponent);
    }
    
    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Полезная ссылка";
        bc.link = "/viewSectionLink/"+this.sectionLink.id;
        bc.type = "viewSectionLink";
        return bc;
    }

    sectionLink: SectionLink = new SectionLink();
    sectionId: number = undefined;

    init(obj:any): any 
    {
        super.init(obj);

        let sectionLinkId:number = -1;
        this.route.params.subscribe(params => {sectionLinkId = +params['id']});

        let sectionLinkPromise = this.sectionLinkService.getById(sectionLinkId).then(
            response => 
            {
                this.sectionLink = response;
                this.sectionId = response.sectionId;
            },
            error => 
            {
                return this.rejectPromise(error);
            }
        );

        return sectionLinkPromise;
    }
    
    editSectionLink(): void 
    {
        this.router.navigate(['/editSectionLink', this.sectionLink.id]);
    }

    deleteSectionLink(): void
    {
        
    }
}