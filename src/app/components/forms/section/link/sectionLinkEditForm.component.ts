import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { Section } from '../../../../classes/section'
import { SectionLink } from '../../../../classes/sectionLink'
import { SectionLinkService } from '../../../../services/sectionLink.service';

import { BreadCrumb } from '../../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../../app.component';
import { BaseViewComponent } from '../../../base/baseView.component';

@Component({
    selector: 'section-link-edit-form',
    template: require('../../../../views/forms/section/link/sectionLinkEditForm.html'),
    providers: [ SectionLinkService ]
})

export class SectionLinkEditFormComponent extends BaseViewComponent 
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public sectionLinkService: SectionLinkService)
    {
        super(router, route, location, appComponent);
    }

    sectionLink: SectionLink = new SectionLink();
    sectionId: number = undefined;

    init(obj:any): any 
    {
        super.init(obj);

        let sectionLinkId:number = -1;
        this.route.params.subscribe(params => {sectionLinkId = +params['id']});

        let sectionLinkPromise = this.sectionLinkService.getById(sectionLinkId).then(
            response => 
            {
                this.sectionLink = response;
                this.sectionId = response.sectionId;
            },
            error =>
            {
                return this.rejectPromise(error);
            }
        );

        return sectionLinkPromise;
    }
    
    saveSectionLink(): void 
    {
        this.sectionLinkService.update(this.sectionLink).then(() => this.goBack());
    }
}