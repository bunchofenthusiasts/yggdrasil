import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { SectionLink } from '../../../classes/sectionLink'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';
import { SectionLinkService } from '../../../services/sectionLink.service';

import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

import { DNDUtil } from '../../../utils/dnd/dndUtil';

@Component({
    selector: 'section-view-form',
    template: require('../../../views/forms/section/sectionViewForm.html'),
    providers: [ SectionService, SectionLinkService ]
})

export class SectionViewFormComponent extends BaseViewComponent
{
    constructor(public router: Router, public route: ActivatedRoute, public location: Location, public appComponent: AppComponent,
        private sectionService: SectionService, private sectionLinkService:SectionLinkService)
    {
        super(router, route, location, appComponent)
    }
    
    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = this.section.name;
        bc.link = "/viewSection/"+this.section.id;
        bc.type = "viewSection";
        return bc;
    }

    trackId: number = undefined;
    section: Section = new Section();
    sectionLinks: SectionLink[];

    init(obj:any): any 
    {
        super.init(obj);

        let sectionId:number = -1;
        this.route.params.subscribe(params => {sectionId = +params['id']});

        let sectionPromise = this.sectionService.getById(sectionId);

        let sectionLinksPromise:Promise<any> = sectionPromise.then(
            response => 
            {
                this.section = response;
                this.trackId = response.trackId;
                return this.sectionLinkService.getList({ sectionId: sectionId });
            },
            error => 
            {
                return this.rejectPromise(error);
            }).then(
                response =>
                {
                    this.sectionLinks = [];
                    response.forEach(
                        (existedLink:SectionLink) =>
                        {
                            let obj:SectionLink = existedLink as SectionLink;
                            this.sectionLinks.push(obj)        
                        });
                },
                error => 
                {
                    return this.rejectPromise(error);
                }
            );

        return sectionLinksPromise;
    }
    
    editSection(): void 
    {
        this.router.navigate(['/editSection', this.section.id]);
    }

    addLinkToSection(): void
    {
        this.router.navigate(['/createSectionLink', this.section.id]);   
    }

    viewSectionLink(sectionLink: SectionLink): void
    {
        this.router.navigate(['/viewSectionLink', sectionLink.id]);
    }

    deleteSectionLink(sectionLink: SectionLink): void
    {
        let freeOrderNumber:number = sectionLink.orderNumber;

        this.sectionLinkService.delete(sectionLink.id)
            .then(() => 
            {
                this.sectionLinks = this.sectionLinks.filter(u => u !== sectionLink);
                DNDUtil.updateOrderedListAfterDeleteRow(freeOrderNumber, this.sectionLinks);
            });
    }

    onDrop(src: SectionLink, trg: SectionLink) 
    {
        DNDUtil.moveRowByOrderNumber(this.sectionLinks, src.orderNumber, trg.orderNumber);
    }

    navigateOnSectionLink(sectionLink: SectionLink): void
    {
        console.log("navigateOnSectionLink");
        window.location.href = sectionLink.href;
    }

}