import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';

import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

@Component({
    selector: 'section-create-form',
    template: require('../../../views/forms/section/sectionEditForm.html'),
    providers: [ SectionService ]
})

export class SectionCreateFormComponent extends BaseViewComponent 
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public sectionService: SectionService)
    {
        super(router, route, location, appComponent);
    }
    
    get currentBreadCrumb():BreadCrumb
    {
        let bc:BreadCrumb = new BreadCrumb();
        bc.name = "Создание новой секции";
        bc.link = "/createSection";    
        bc.type = "createSection";
        return bc;
    } 

    section: Section = new Section();
    trackId: number = undefined;
    sectionsCount: number = undefined;

    init(obj:any): any 
    {
        super.init(obj);

        this.route.params.subscribe(params => {this.trackId = +params['trackId']});       

        let sectionPromise = this.sectionService.getList({ trackId: this.trackId}).then(
            response => 
            {
                this.sectionsCount = response.length;
            },
            error =>
            {
                return this.rejectPromise(error);
            });
        return sectionPromise;
    }
    
    saveSection(): void 
    {
        this.section.trackId = this.trackId;
        this.section.isPassed = false;
        this.section.orderNumber = this.sectionsCount; // Перенести на сервер присвоение номера
        this.sectionService.create(this.section).then(() => this.goBack());
    }
}