import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Location } from '@angular/common';
import { User } from '../../../classes/user'
import { Track } from '../../../classes/track'
import { Section } from '../../../classes/section'
import { TrackService } from '../../../services/track.service';
import { SectionService } from '../../../services/section.service';
import { BreadCrumb } from '../../../classes/view/breadCrumbs/breadCrumb';

import { AppComponent } from '../../app.component';
import { BaseViewComponent } from '../../base/baseView.component';

@Component({
    selector: 'section-edit-form',
    template: require('../../../views/forms/section/sectionEditForm.html'),
    providers: [ SectionService ]
})

export class SectionEditFormComponent extends BaseViewComponent 
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public sectionService: SectionService)
    {
        super(router, route, location, appComponent);
    }

    section: Section = new Section();
    trackId: number = undefined;

    init(obj:any): any 
    {
        super.init(obj);

        let sectionId:number = -1;
        this.route.params.subscribe(params => {sectionId = +params['id']});

        let sectionPromise = this.sectionService.getById(sectionId).then(
            response => 
            {
                this.section = response;
                this.trackId = response.trackId;
            },
            error =>
            {
                return this.rejectPromise(error);
            }
        );

        return sectionPromise;
    }
    
    saveSection(): void 
    {
        this.sectionService.update(this.section).then(() => this.goBack());
    }
}