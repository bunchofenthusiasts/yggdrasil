import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Http, JsonpModule } from '@angular/http';
import { UserService } from '../../services/user.service';
import { User } from '../../classes/user'

import { ContentHeaders } from '../../common/headers/headers';

import { AppComponent } from '../app.component';
import { BaseViewComponent } from '../base/baseView.component';

@Component({
    selector: 'sign-in',
    template: require('../../views/auth/signIn.html')
})

export class SignInComponent extends BaseViewComponent
{
    constructor(public router: Router, 
                public route: ActivatedRoute,
                public location: Location,
                public http: Http,
                public userService: UserService,
                public appComponent: AppComponent
                ) 
    {
        super(router, route, location, appComponent);
    }
    
    users: User[];

    init(obj:any): any 
    {
        var delayPromise:Promise<any> = super.init(obj);

        var usersPromise:Promise<any> = delayPromise.then(
            () =>
            {
                return this.userService.getList();                
            }).then(
                response =>
                {
                    this.users = response;
                },
                error => 
                {
                    return this.rejectPromise(error);
                });
        
        return usersPromise;
    }

    signIn(event, username, password) 
    {
        event.preventDefault();
        let body = JSON.stringify({ username, password });
        
        this.http.post('api/sessions/create', body, { headers: ContentHeaders })
        .subscribe(
            response => 
            {
                this.appComponent.login(response);
                this.router.navigate(['/dashboard/', response.json().user.id ]);
            },
            error => this.navigateToErrorPage(error)
        );
    }
}