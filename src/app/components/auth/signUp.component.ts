import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Http } from '@angular/http';

import { ContentHeaders } from '../../common/headers/headers';

import { AppComponent } from '../app.component';
import { BaseComponent } from '../base/base.component';
import { BaseViewComponent } from '../base/baseView.component';

@Component({
    selector: 'sign-up',
    template: require('../../views/auth/signUp.html')
})
export class SignUpComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent,
        public http: Http)
    {
        super(router, route, location, appComponent);
    }

    error: string = undefined;

    signUp(event, username, password, firstName, secondName, email) 
    {
        event.preventDefault();
        let body = JSON.stringify({ username, password, firstName, secondName, email });
        
        this.http.post('api/signUp', body, { headers: ContentHeaders })
        .subscribe(
            response => 
            {
                this.appComponent.login(response);
                this.router.navigate(['/dashboard/', response.json().user.id ]);
            },
            error => this.navigateToErrorPage(error)
        );
    }
}