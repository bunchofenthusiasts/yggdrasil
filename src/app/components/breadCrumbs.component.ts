import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { BaseComponent } from './base/base.component';
import { BreadCrumb } from '../classes/view/breadCrumbs/breadCrumb';

@Component({
    selector: 'bread-crumbs',
    template: require('../views/breadCrumbs.html')
})

export class BreadCrumbsComponent implements OnInit 
{

    constructor(public router: Router) 
    {
        this._router = router;
    }

    private _router:Router;
    private static _instance:BreadCrumbsComponent;

    public static get instance():BreadCrumbsComponent
    {
        if (BreadCrumbsComponent._instance == null)
            BreadCrumbsComponent._instance = new BreadCrumbsComponent(BreadCrumbsComponent._instance._router);

        return BreadCrumbsComponent._instance;
    }


    public breadCrumbsList:Array<BreadCrumb> = [];
    public activeBreadCrumb:BreadCrumb = undefined;

    ngOnInit(): void 
    {
        this.loadFromStorage();

        if (this.activeBreadCrumb == null)
            this.init();
            
        BreadCrumbsComponent._instance = this;
    }

    private init():void
    {
        this.breadCrumbsList = [];
        this.activeBreadCrumb = undefined;

        let home:BreadCrumb = new BreadCrumb();
        home.name = "Главная";
        home.link = "/home";
        home.type = "home";
    
        this.addBreadCrumb(home);
    }

    navigateToLink(breadCrumb:BreadCrumb): void
    {
        if (breadCrumb == null)
            return;

        this.router.navigate([breadCrumb.link]);
    }

    public clearBreadCrumbs():void
    {
        this.init();
    }

    public addBreadCrumb(newBreadCrumb:BreadCrumb): void
    {
        if (newBreadCrumb == null)
            return;

        let existed:BreadCrumb = this.breadCrumbsList.find(breadCrumb => breadCrumb.type === newBreadCrumb.type);

        if ((this.activeBreadCrumb != null) && (this.activeBreadCrumb.type == newBreadCrumb.type))
        {
            this.activeBreadCrumb = newBreadCrumb;
        
        }
        else if (existed != undefined)
        {
            let existedIndex:number = this.breadCrumbsList.indexOf(existed);

            for (let i = this.breadCrumbsList.length; i > 0; i--)
            {
                if (i > existedIndex)
                {
                    this.breadCrumbsList.pop();
                }
            }

            this.activeBreadCrumb = newBreadCrumb;
        
        }
        else 
        {
            if (this.activeBreadCrumb != null)
                this.breadCrumbsList.push(this.activeBreadCrumb);

            this.activeBreadCrumb = newBreadCrumb;
        }

        this.saveToStorage();
    }

    private saveToStorage(): void
    {
        localStorage.setItem("activeBreadCrumb", JSON.stringify(this.activeBreadCrumb));
        localStorage.setItem("breadCrumbsList", JSON.stringify(this.breadCrumbsList));
    }

    private loadFromStorage(): void
    {
        this.activeBreadCrumb = JSON.parse(localStorage.getItem("activeBreadCrumb"));
        this.breadCrumbsList = JSON.parse(localStorage.getItem("breadCrumbsList"));
    }
}