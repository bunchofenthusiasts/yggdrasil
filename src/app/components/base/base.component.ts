import { Component, OnInit } from '@angular/core';
import { ErrorPageComponent } from '../system/errorPage.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Rx';

export class BaseComponent implements OnInit
{
  
  constructor(public router: Router, public route: ActivatedRoute, public location: Location) {}

  private promise:Promise<any>;

  ngOnInit(): void 
  {
    this.promise = new Promise(function(resolve, reject)
    {
      resolve();
    });

    this.promise.then(this.beforeInit.bind(this)).then(this.init.bind(this)).then(this.afterInit.bind(this))
  }

  beforeInit(obj:any):any
  {
    //console.log("beforeInit");
  }

  init(obj:any): any 
  {
    //console.log("init");
    return Observable.timer(1000).toPromise()
  }

  afterInit(obj:any): any 
  {   
    //console.log("afterInit");
  }

  navigateToErrorPage(error:Object) 
  {
    let errorText:string = "";
    if (error instanceof Response )
    {
      errorText = error.text();
    }

    localStorage.setItem('lastError', errorText);
    this.router.navigate(['/error']);
  }

  rejectPromise(error: any): Promise<any> 
  {
    this.navigateToErrorPage(error);
    return Promise.reject(error.message || error);
  }

  goBack(): void 
  {
    this.location.back();
  }
}
