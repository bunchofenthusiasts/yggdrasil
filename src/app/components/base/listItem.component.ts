import { Component, OnInit, Input, Output, ElementRef, EventEmitter  } from '@angular/core';
import { IListItem } from '../../classes/interfaces/iListItem';

@Component({
  selector: 'list-item',
  template: require('../../views/base/listItem.html')
})
export class ListItem implements OnInit 
{
  @Input() item:IListItem;
  @Input() needShowOrderIndex:Boolean = true;
  @Input() elementSpanStyles:string[];

  @Output() click = new EventEmitter();
  @Output() dblClick = new EventEmitter();
  @Output() delete = new EventEmitter();

  @Output() elementClick = new EventEmitter();

  onDblClick()
  {
    this.dblClick.emit(this.item);
  }
  
  onDelete()
  {
    this.delete.emit(this.item);
  }

  onClick()
  {
    console.log("onClick");
    this.click.emit(this.item);
  }

  onElementClick()
  {
    console.log("onElementClick");
    console.log(this.item);
    this.elementClick.emit(this.item);
  }

  constructor() {}

  ngOnInit() {
    console.log(this.elementSpanStyles)
  }
}
