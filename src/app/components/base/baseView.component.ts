import { Component, OnInit } from '@angular/core';
import { ErrorPageComponent } from '../system/errorPage.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import { Location } from '@angular/common';
import { BaseComponent } from './base.component';
import { BreadCrumbsComponent } from '../breadCrumbs.component';
import { BreadCrumb } from '../../classes/view/breadCrumbs/breadCrumb';
import { AppComponent } from '../app.component';

export class BaseViewComponent extends BaseComponent
{
  
    constructor(public router: Router, public route: ActivatedRoute, public location: Location, public appComponent: AppComponent) 
    {
        super(router, route, location)
    }

    get currentBreadCrumb():BreadCrumb
    {
        return undefined;
    }

    
    beforeInit(obj:any):any
    {
        super.beforeInit(obj);
        this.appComponent.showLoader();
    }

    afterInit(obj:any): any 
    {   
        super.afterInit(obj);

        console.log("addBreadCrumb start");
        console.log(this.currentBreadCrumb);
        BreadCrumbsComponent.instance.addBreadCrumb(this.currentBreadCrumb);
        console.log("addBreadCrumb end ");

        this.appComponent.hideLoader();
    } 
}
