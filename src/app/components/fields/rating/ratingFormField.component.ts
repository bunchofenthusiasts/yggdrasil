import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { BaseComponent } from '../../base/base.component';
import { AppComponent } from '../../app.component';
import { Timer } from '../../../utils/timer';
import { ObjectUserRatingService } from '../../../services/objectUserRating.service';
import { ObjectUserRating } from '../../../classes/rating/objectUserRating';
import { Entity } from '../../../classes/entity';


@Component({
    selector: 'rating-form-field',
    template: require('../../../views/fields/rating/ratingFormField.html'),
    providers: [ ObjectUserRatingService ]
    
})

export class RatingFormFieldComponent extends BaseComponent implements OnChanges
{
    constructor(public router: Router, public route: ActivatedRoute, public location: Location,
        public appComponent: AppComponent,
        public objectUserRatingService: ObjectUserRatingService)
    {
        super(router, route, location);
    }

    @Input() userId:number = this.appComponent.currentUserId;

    @Input() object:Entity;

    @Input() isItEditMode:Boolean;

    @Input() marks:Array<any> = [
        {value:1, checked:false},
        {value:2, checked:false},
        {value:3, checked:false},
        {value:4, checked:false},
        {value:5, checked:false},
    ]

    allUserObjectRatings:ObjectUserRating[] = undefined;

    objectUserRating:ObjectUserRating = undefined;

    @Input() averageRatingValue:number = 0;

    @Input() userRatingValue:number = 0;

    ngOnChanges(changes: SimpleChanges)
    {
        if (changes["object"] != undefined && changes["object"]["currentValue"] != undefined)
           this.calcRating();
    }

   init(obj:any): any 
   {
        
   }

        
    getMarkByValue(value:number): Object
    {
        return this.marks.find(mark => mark.value === value);
    }

    clearMarks():void
    {
        this.marks.forEach(mark => mark.checked = false);
    }

    editObjectUserRating(): void 
    {
        this.isItEditMode = true;
    }

    onObjectUserRatingChange(mark): void 
    {
        this.clearMarks();

        mark.checked = true;

        let savePromise:Promise<any> = undefined;
        if (this.objectUserRating != undefined)
        {
            console.log(this.objectUserRating);
            this.objectUserRating.userMark = mark.value;
            savePromise = this.objectUserRatingService.update(this.objectUserRating);
        }
        else
        {
            this.objectUserRating = new ObjectUserRating();
            this.objectUserRating.userId = this.userId;
            this.objectUserRating.userMark = mark.value;
            this.objectUserRating.objectType = this.object.constructor.name;
            this.objectUserRating.objectId = this.object.id;
            
            savePromise = this.objectUserRatingService.create(this.objectUserRating);
        }

        savePromise.then(
            response => this.calcRating().then(() => Timer.runAfterDelay(1000, () => this.isItEditMode = false)),
            error => this.rejectPromise(error)
        )

        
    }

    calcRating() :Promise<any>
    {
        if (this.object == undefined)
            return;

        return this.objectUserRatingService.getList({ objectType: this.object.constructor.name, objectId: this.object.id }).then(
            response =>
            {
                this.allUserObjectRatings = [];
                this.averageRatingValue = 0;
                response.forEach(
                    (userRating:ObjectUserRating) =>
                    {
                        this.averageRatingValue += userRating.userMark;
                        this.allUserObjectRatings.push(userRating);

                        if (userRating.userId == this.userId)
                        {
                            this.objectUserRating = userRating;
                            this.userRatingValue = userRating.userMark;

                            this.getMarkByValue(this.userRatingValue)["checked"] = true;
                        }
                    });

                    if (response.length > 0)
                    {
                        this.averageRatingValue /= response.length;
                    }
                    else
                    {
                        this.averageRatingValue = 0;
                    }
            },
            error => this.rejectPromise(error)
        );
    }
}