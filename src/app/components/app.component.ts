﻿import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../classes/user'
import { Track } from '../classes/track'
import { UserService } from '../services/user.service';
import { BreadCrumbsComponent } from './breadCrumbs.component';

@Component({
    selector: 'my-app',
    template: require('../views/main.html'),
    providers: [UserService]
})

export class AppComponent implements OnInit {

    constructor(
        private userService: UserService,
        private router: Router) { }

    tracks: Track[];

    users: User[];

    @Input()
    public isLoaderVisible:Boolean = false;

    @Input()
    contentDivStyles:string[];

    get currentUserId(): number
    {
        if (this.currentUser)
            return this.currentUser.id;
        
        let idFromSession = localStorage.getItem("currentUserId");
        if (idFromSession != undefined)
            return +idFromSession;
        
        return undefined;
    }

    @Input()
    public currentUser:User = undefined;

    ngOnInit(): void 
    {
        if (this.currentUserId != undefined && this.currentUser == undefined)
        {
            this.userService.getById(this.currentUserId).then(
                response =>
                {
                    this.currentUser = response;
                },   
                error => 
                {
                    console.log(error);
                });
        }
    }

    signIn() 
    {
        this.router.navigate(['/signIn']);
    }

    signUp() 
    {
        this.router.navigate(['/signUp']);
    }

    login(response) 
    {
        localStorage.setItem('id_token', response.json().id_token);
        localStorage.setItem('currentUserId', response.json().user.id);

        this.currentUser = response.json().user;
    }

    logout() 
    {
        localStorage.removeItem('id_token');
        localStorage.removeItem('currentUserId');
        
        this.currentUser = undefined;

        BreadCrumbsComponent.instance.clearBreadCrumbs();

     //   this.breadCrumbs.clearBreadCrumbs();

        this.router.navigate(['signIn']);
    } 

    navigateToHome()
    {
        this.router.navigate(['/home']);  
    }

    navigateToDashboard()
    {
        var currentUserId:String = localStorage.getItem("currentUserId");
        this.router.navigate(['/dashboard/', currentUserId ]);
    }  

    showLoader():void
    {
        this.isLoaderVisible = true;
        this.contentDivStyles = ['blur'];
    }
    
    hideLoader():void
    {
        this.isLoaderVisible = false;
        this.contentDivStyles = [];
    }
}


