import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AppComponent } from '../app.component';
import { BaseViewComponent } from '../base/baseView.component';

@Component({
    selector: 'not-found-page',
    template: require('../../views/system/notFound.html')
})

export class NotFoundPageComponent extends BaseViewComponent
{
    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent)
    {
        super(router, route, location, appComponent);
    }

    goBack(): void 
    {
        this.location.back();
    }

}