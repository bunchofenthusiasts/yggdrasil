import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { BaseViewComponent } from '../base/baseView.component';
import { AppComponent } from '../app.component';

@Component({
    selector: 'error-page',
    template: require('../../views/system/error.html')
})

export class ErrorPageComponent extends BaseViewComponent
{

    constructor(
        public router: Router, 
        public route: ActivatedRoute,
        public location: Location,
        public appComponent: AppComponent)
    {
        super(router, route, location, appComponent);
    }

    errorText: string = undefined;

    beforeInit(obj:any):any
    {
        super.beforeInit(obj);
         this.errorText = localStorage.getItem("lastError");
        //console.log("Error was ocured: " + this.errorText);

        if (!this.errorText)
        {
            //console.log("Last error doesn't exist! Go back!")
            this.router.navigate(['/home']);
        }
    }

    goBack(): void 
    {
        localStorage.removeItem("lastError");
        this.location.back();
    }

}