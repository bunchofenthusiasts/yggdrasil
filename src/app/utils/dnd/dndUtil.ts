import { IListItem } from '../../classes/interfaces/iListItem';

export class DNDUtil
{
    static moveRowByOrderNumber(dataArray:IListItem[], sourceOrderNumber:number, targetOrderNumber:number) :void
    {
        if (sourceOrderNumber == undefined || targetOrderNumber == undefined)
            return;

        if (sourceOrderNumber == targetOrderNumber)
            return;

        if (sourceOrderNumber > targetOrderNumber) 
        {
            for (let i = targetOrderNumber; i < sourceOrderNumber; i++) 
            {
                dataArray[i].orderNumber++;
            }
        } 
        else
        {  
            for (let i = sourceOrderNumber + 1; i <= targetOrderNumber; i++) 
            {
                dataArray[i].orderNumber--;
            }
        }
    
        dataArray[sourceOrderNumber].orderNumber = targetOrderNumber;
        dataArray.sort((a, b) => a.orderNumber - b.orderNumber);
    }

    static updateOrderedListAfterDeleteRow(freeOrderNumber:number, dataArray:IListItem[]):void
    {
        dataArray.forEach((elem) =>
        {
            if (elem.orderNumber > freeOrderNumber)
                elem.orderNumber--;
        });
    }
}