import { Observable } from 'rxjs/Rx';

export class Timer
{
	static runAfterDelay(msecDelay:number, onTimerComplete:Function): Promise<any>
    {
        return Observable.timer(msecDelay).toPromise().then(() => onTimerComplete(), error => Promise.reject(error.message || error));
    }
}