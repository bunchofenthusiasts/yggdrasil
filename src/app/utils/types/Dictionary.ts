
export class Dictionary<T> 
{
    private _items: { [index: string]: T } = {};
 
    private _count: number = 0;
 
    public hasKey(key: string): boolean 
    {
        return this._items.hasOwnProperty(key);
    }
 
    public count(): number 
    {
        return this._count;
    }
 
    public add(key: string, value: T) 
    {
        this._items[key] = value;
        this._count++;
    }
 
    public remove(key: string): T 
    {
        var val = this._items[key];
        delete this._items[key];
        this._count--;
        return val;
    }
 
    public get(key: string): T 
    {
        return this._items[key];
    }
 
    public keys(): string[] 
    {
        var keySet: string[] = [];
 
        for (var prop in this._items) 
        {
            if (this._items.hasOwnProperty(prop)) 
            {
                keySet.push(prop);
            }
        }
 
        return keySet;
    }
 
    public values(): T[] 
    {
        var result: T[] = [];
 
        for (var prop in this._items) 
        {
            if (this._items.hasOwnProperty(prop)) 
            {
                result.push(this._items[prop]);
            }
        }
 
        return result;
    }
}