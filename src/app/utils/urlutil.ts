export class URLUtil
{
	static objectToUrlParams(filterObject: Object): string {
		if (!filterObject)
			return '';

		let properties: Array<string> = [];

		for (let property in filterObject)
			properties.push(property + '=' + filterObject[property]);

		let result = properties.join('&');

		if (result !== '')
			result = '?' + result;

		return result;
    }
}