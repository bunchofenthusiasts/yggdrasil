import { Entity } from './entity';

export class Comment extends Entity {
	message: string;
	createdOn: Date;
	sectionId: number;
}