import { Entity } from './entity';

export class TimeEffort extends Entity {
	sectionId: number;
	startTime: number;
	finishTime: number;
}