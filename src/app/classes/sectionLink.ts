import { URL } from './common/url';
import { IListItem } from './interfaces/iListItem';

export class SectionLink extends URL implements IListItem
{
	sectionId: number;
	orderNumber: number;
}