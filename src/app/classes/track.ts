import { Entity } from './entity';

export class Track extends Entity {
	name: string;
	description: string;
	isPassed: boolean;
	userId: number;
}