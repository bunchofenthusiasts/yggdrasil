import { Entity } from './entity';

export class User extends Entity {
	name: string;
	email: string;
	firstName: string;
	secondName: string;
	password: string;
}