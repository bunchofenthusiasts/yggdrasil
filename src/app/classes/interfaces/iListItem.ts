export interface IListItem 
{
    id: number;
    orderNumber: number;

    toString():string;
}   