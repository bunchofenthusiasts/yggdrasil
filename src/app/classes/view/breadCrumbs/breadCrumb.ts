export class BreadCrumb
{
	name: string;
	link: string;
	type: string;
	isActive: boolean;
}