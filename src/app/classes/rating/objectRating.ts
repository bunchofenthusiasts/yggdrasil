import { Entity } from '../entity';

export class ObjectRating extends Entity 
{
	objectId: number;
    objectType: string;
	averageMark: number;
	countMarks: number;
}