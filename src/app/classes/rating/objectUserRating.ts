import { Entity } from '../entity';

export class ObjectUserRating extends Entity 
{
	objectId: number;
    objectType: string;
    userId: number;
	userMark: number;
}