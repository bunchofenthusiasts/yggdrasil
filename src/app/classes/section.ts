import { Entity } from './entity';
import { IListItem } from './interfaces/iListItem';

export class Section extends Entity implements IListItem 
{
	name: string;
	description: string;
	isPassed: boolean;
	trackId: number;
	estimate: number;
	timespent: number;
	// Порядковый номер в тропинке
	orderNumber: number;

	toString():string
	{
		var orderString:string = "";
		if (this.orderNumber != undefined &&  !isNaN(this.orderNumber))
			orderString = this.orderNumber.toString();

		return ("toString: " + orderString  + ". " + this.name + " " + this.description);
	}
}