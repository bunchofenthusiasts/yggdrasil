import { Entity } from '../entity';

export class URL extends Entity 
{
	href: string;
	description: string;	
}