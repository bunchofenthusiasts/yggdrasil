﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { HttpModule }    from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './db/in-memory-data.service';

import { AUTH_PROVIDERS } from 'angular2-jwt';
import { AuthGuardComponent } from './components/auth/authGuard.component';
import { SignInComponent } from './components/auth/signIn.component';
import { SignUpComponent } from './components/auth/signUp.component';

import { AppComponent }   from './components/app.component';
import { HomeComponent }   from './components/home.component';
import { DashboardComponent }   from './components/dashboard.component';
import { TrackViewFormComponent } from './components/forms/track/trackViewForm.component';
import { TrackEditFormComponent }   from './components/forms/track/trackEditForm.component';
import { TrackCreateFormComponent }   from './components/forms/track/trackCreateForm.component';

import { SectionViewFormComponent }   from './components/forms/section/sectionViewForm.component';
import { SectionEditFormComponent }   from './components/forms/section/sectionEditForm.component';
import { SectionCreateFormComponent }   from './components/forms/section/sectionCreateForm.component';

import { SectionLinkViewFormComponent }   from './components/forms/section/link/sectionLinkViewForm.component';
import { SectionLinkEditFormComponent }   from './components/forms/section/link/sectionLinkEditForm.component';
import { SectionLinkCreateFormComponent }   from './components/forms/section/link/sectionLinkCreateForm.component';

import { ErrorPageComponent } from './components/system/errorPage.component';
import { NotFoundPageComponent }   from './components/system/notFoundPage.component';
import { BreadCrumbsComponent } from './components/breadCrumbs.component';
import { BaseViewComponent } from './components/base/baseView.component';

import { MakeDraggable } from './utils/dnd/make-draggable.directive';
import { MakeDroppable } from './utils/dnd/make-droppable.directive';
import { ListItem } from './components/base/listItem.component';

import { AppRoutingModule }     from './app-routing.module';

import { Section }     from './classes/section';

import { RatingFormFieldComponent } from './components/fields/rating/ratingFormField.component';


@NgModule({
  imports:      
  [ 
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {passThruUnknownUrl: true})
  ],
  declarations:
  [
    AppComponent, HomeComponent, SignInComponent, SignUpComponent, ErrorPageComponent, NotFoundPageComponent,
    DashboardComponent, TrackViewFormComponent, TrackEditFormComponent, TrackCreateFormComponent,
    SectionEditFormComponent, SectionViewFormComponent, SectionCreateFormComponent,
    SectionLinkEditFormComponent, SectionLinkViewFormComponent, SectionLinkCreateFormComponent,
    MakeDraggable, MakeDroppable, ListItem, BreadCrumbsComponent,
    RatingFormFieldComponent
  ],
  bootstrap:
  [
    AppComponent
  ],
  providers:
  [
    AuthGuardComponent, ...AUTH_PROVIDERS 
  ]
})
export class AppModule { }
