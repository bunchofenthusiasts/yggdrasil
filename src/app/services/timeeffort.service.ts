import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { TimeEffort } from '../classes/timeeffort';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class TimeEffortService extends BaseService<TimeEffort>
{
    protected serviceURL = 'app/timeEfforts';

    constructor(public http: Http)
    {
        super(http);
    }
}