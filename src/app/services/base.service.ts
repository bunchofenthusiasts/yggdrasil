import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { ContentHeaders } from '../common/headers/headers';

import 'rxjs/add/operator/toPromise';

import { Entity } from '../classes/entity';
import { URLUtil } from '../utils/urlutil';

@Injectable()
export class BaseService<T extends Entity>
{
    protected serviceURL = '';
    
    constructor(public http: Http)
    {
    }

    getList(filter:Object = null):Promise<T[]>
    {
        const url = `${this.serviceURL}/${URLUtil.objectToUrlParams(filter)}`;
        
        return this.http.get(url)
               .toPromise()
               .then(response => this.parseObjList(response.json().data as T[]))
               .catch(this.handleError);      
    }

    getById(id:number):Promise<T>
    {
        const url = `${this.serviceURL}/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => this.parseObj(response.json().data))
            .catch(this.handleError);
    }

    create(object: T): Promise<T> 
    {
        return this.http.post(this.serviceURL, JSON.stringify(object), {headers: ContentHeaders})
            .toPromise()
            .then(res => this.parseObj(res.json().data))
            .catch(this.handleError);
    }

    update(object: T): Promise<T> 
    {
        const url = `${this.serviceURL}/${object.id}`;

        return this.http.put(url, JSON.stringify(object), {headers: ContentHeaders})
            .toPromise()
            .then(() => object)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> 
    {
        const url = `${this.serviceURL}/${id}`;

        return this.http.delete(url, {headers: ContentHeaders})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    protected handleError(error: any): Promise<any> 
    {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
    
    protected parseObj(object:Object):any
    {
        console.error('Please, override this method!');
        return undefined;
    }

    protected parseObjList(list:Array<Object>):T[]
    {
        let result:T[] = [];
        list.forEach(obj => result.push(this.parseObj(obj)))
        return result;
    }
}