import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Section } from '../classes/section';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class SectionService extends BaseService<Section>
{
    protected serviceURL = 'app/sections';

    constructor(public http: Http)
    {
        super(http);
    }
    
    parseObj(object:Object):Section
    {
        return new Section().fromJSON(object);
    }
}