import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { User } from '../classes/user';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class UserService extends BaseService<User>
{
    protected serviceURL = 'api/devs';

    constructor(public http: Http)
    {
        super(http);
    }

    parseObj(object:Object):User
    {
        return new User().fromJSON(object);
    }    
}