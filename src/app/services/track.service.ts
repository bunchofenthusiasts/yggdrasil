import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Track } from '../classes/track';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class TrackService extends BaseService<Track>
{
    protected serviceURL = 'app/tracks';

    constructor(public http: Http)
    {
        super(http);
    }

    parseObj(object:Object):Track
    {
        return new Track().fromJSON(object);
    }       
}