import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Comment } from '../classes/comment';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class CommentService extends BaseService<Comment>
{
    protected serviceURL = 'app/comments';

    constructor(public http: Http)
    {
        super(http);
    }
}