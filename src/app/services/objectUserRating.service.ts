import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { ObjectUserRating } from '../classes/rating/objectUserRating';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class ObjectUserRatingService extends BaseService<ObjectUserRating>
{
    protected serviceURL = 'app/objectUserRatings';

    constructor(public http: Http)
    {
        super(http);
    }
    
    parseObj(object:Object):ObjectUserRating
    {
        return new ObjectUserRating().fromJSON(object);
    }
}