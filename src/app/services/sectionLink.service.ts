import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { SectionLink } from '../classes/sectionLink';
import { BaseService } from './base.service';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class SectionLinkService extends BaseService<SectionLink>
{
    protected serviceURL = 'app/sectionLinks';

    constructor(public http: Http)
    {
        super(http);
    }

    parseObj(object:Object):SectionLink
    {
        return new SectionLink().fromJSON(object);
    }
}