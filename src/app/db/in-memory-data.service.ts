import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService 
{
  createDb() 
  {
    let tracks =  
    [
        {
            id: 1,
            name: 'Задача 1',
            description: 'Описание задачи 1',
            isPassed: true,
            userId: 1
        },
        {
            id: 2,
            name: 'Задача 2',
            description: 'Описание задачи 2',
            isPassed: true,
            userId: 1
        },
        {
            id: 3,
            name: 'Задача 3',
            description: 'Описание задачи 3',
            isPassed: false,
            userId: 1
        },
        {
            id: 4,
            name: 'Задача 4',
            description: 'Описание задачи 4',
            isPassed: false,
            userId: 1
        },
        {
            id: 5,
            name: 'Задача 5',
            description: 'Описание задачи 5',
            isPassed: true,
            userId: 2
        },
        {
            id: 6,
            name: 'Задача 6',
            description: 'Описание задачи 6',
            isPassed: false,
            userId: 2
        },
        {
            id: 7,
            name: 'Задача 7',
            description: 'Описание задачи 7',
            isPassed: false,
            userId: 2
        },{
            id: 8,
            name: 'Задача 8',
            description: 'Описание задачи 8',
            isPassed: false,
            userId: 2
        },
        {
            id: 9,
            name: 'Задача 9',
            description: 'Описание задачи 9',
            isPassed: true,
            userId: 3
        },
        {
            id: 10,
            name: 'Задача 10',
            description: 'Описание задачи 10',
            isPassed: false,
            userId: 3
        },
    ];

    let sections =
    [
        {
            id: 1,
            name: 'Секция 1',
            description: 'Описание секции 1',
            isPassed: false,
            trackId: 1,
            estimate: 10,
            timespent: 10,
            orderNumber: 0
        },
        {
            id: 2,
            name: 'Секция 2',
            description: 'Описание секции 2',
            isPassed: false,
            trackId: 1,
            estimate: 20,
            timespent: 20,
            orderNumber: 1
        },
        {
            id: 3,
            name: 'Секция 3',
            description: 'Описание секции 3',
            isPassed: false,
            trackId: 1,
            estimate: 15,
            timespent: 15,
            orderNumber: 2
        },
        {
            id: 4,
            name: 'Секция 4',
            description: 'Описание секции 4',
            isPassed: false,
            trackId: 2,
            estimate: 12,
            timespent: 12,
            orderNumber: 0
        },
        {
            id: 5,
            name: 'Секция 5',
            description: 'Описание секции 5',
            isPassed: false,
            trackId: 2,
            estimate: 0,
            timespent: 0,
            orderNumber: 1
        },
    ];

    let comments =
    [
        {
            id: 1,
            message: 'Комменарий 1 к секции 1',
            createdOn: new Date(2016, 11, 1),
            sectionId: 1
        },
        {
            id: 2,
            message: 'Комменарий 2 к секции 1',
            createdOn: new Date(2016, 11, 2),
            sectionId: 1
        },
        {
            id: 3,
            message: 'Комменарий 3 к секции 2',
            createdOn: new Date(2016, 11, 3),
            sectionId: 2
        },
        {
            id: 4,
            message: 'Комменарий 4 к секции 3',
            createdOn: new Date(2016, 11, 4),
            sectionId: 3
        },
    ];

    let sectionLinks =
    [
        {
            id: 1,
            href: 'http://any-site.ru',
            description: 'Ссылка на видяшки',
            sectionId: 1,
            orderNumber: 0
        },
        {
            id: 2,
            href: 'http://other-site.com',
            description: 'Ссылка на курс',
            sectionId: 1,
            orderNumber: 1
        },
    ];

    let timeEfforts =
    [
        {
            id: 1,
            sectionId: 1,
            startTime: 0,
            finishTime: 100
        },
        {
            id: 2,
            sectionId: 1,
            startTime: 200,
            finishTime: 350
        },
        {
            id: 3,
            sectionId: 1,
            startTime: 541,
            finishTime: 1000
        },
    ];

    let objectUserRatings =
    [
        {
            id: 1,
            objectId: 1,
            objectType: "Track",
            userId: 1,
	        userMark: 5
        },
        {
            id: 2,
            objectId: 1,
            objectType: "Track",
            userId: 2,
	        userMark: 4
        },
        {
            id: 3,
            objectId: 1,
            objectType: "Track",
            userId: 3,
	        userMark: 2
        },
        {
            id: 4,
            objectId: 1,
            objectType: "Track",
            userId: 4,
	        userMark: 3
        }
    ];


    return { tracks, sections, comments, sectionLinks, timeEfforts, objectUserRatings };
  }
}