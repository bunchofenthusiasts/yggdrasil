const express = require('express');
const _ = require('lodash');
const router = express.Router();
const config = require('../tokenConfig');
const jwt     = require('jsonwebtoken');
const users     = require('../db/users');

router.all('/test', (req, res) => 
{
  //res.send('api works');
  res.status(200).json({'message':'succeed!'});
});

router.post('/sessions/create', function(req, res) 
{
  if (!req.body.username || !req.body.password) 
  {
    return res.status(400).send("You must send the username and the password");
  }

  var user = _.find(users, {name: req.body.username});
  if (!user) 
  {
    return res.status(401).send("The username or password don't match");
  }

  if (!(user.password === req.body.password)) 
  {
    return res.status(401).send("The username or password don't match");
  }

  res.status(201).send(
    {
      id_token: createToken(user),
      user: user
    });
});

router.all('/devs', function(req, res) 
{
  res.status(201).send({data: users});
});

router.get('/devs/:id', function(req, res) 
{  
  var user = _.find(users, function(_user) { return _user.id == req.params.id;});
  if (!user) 
  {
    return res.status(400).send("The username with id " + req.params.id + " doesn't exist!");
  }

  res.status(201).send(
    {
      data: user
    });
});

router.post('/signUp', function(req, res) 
{
  if (!req.body.username || !req.body.password) 
    return res.status(400).send("You must send the username and the password");
  
  if (_.find(users, {name: req.body.username})) 
   return res.status(400).send("A user with that username already exists");
   
   var newUser = {
     'id': users.length+1,
     'name': req.body.username,
     'email': req.body.email,
     'firstName': req.body.firstName,
     'secondName': req.body.secondName,
     'password': req.body.password
   }

  users.push(newUser);

  res.status(201).send(
    {
      id_token: createToken(newUser),
      user: newUser
    });
});


function createToken(user) 
{
  return jwt.sign(
    _.omit(user, 'password'), config.secret, { expiresIn: 60*5 });
}

module.exports = router;